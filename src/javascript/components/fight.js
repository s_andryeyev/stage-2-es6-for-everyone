import {controls} from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
    });
}

export function getDamage(attacker, defender) {
    // return damage
    // getHitPower - getBlockPower, (або ж 0)
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    // return hit power
    // power = attack * criticalHitChance
    const criticalHitChance = Math.random() + 1;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    // return block power
    // power = defense * dodgeChance
    const dodgeChance = Math.random() + 1;
    return fighter.defense * dodgeChance;
}
