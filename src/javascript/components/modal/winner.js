import {showModal} from './modal'

export function showWinnerModal(fighter) {
    const winner = {
        title: "I'm the Winner!!!!",
        bodyElement: fighter.name
    };
    showModal(winner);
}
