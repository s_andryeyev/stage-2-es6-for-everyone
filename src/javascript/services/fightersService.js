import {callApi} from '../helpers/apiHelper';

class FighterService {
    #endpoint = 'fighters.json';

    async getFighters() {
        try {
            const apiResult = await callApi(this.#endpoint);
            return apiResult;
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id) {
        // todo: implement this method
        try {
            const endpoint = `details/fighter/${id}.json`;
 const res = await callApi(endpoint);
 console.log(res);
            return res;
         } catch (e) {
            throw e;
        }
    }
}

export const fighterService = new FighterService();
