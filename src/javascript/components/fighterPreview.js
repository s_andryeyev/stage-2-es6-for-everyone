import {createElement} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    // todo: show fighter info (image, name, health, etc.)
    function fighterProperty(value) {
        const nameElement = createElement({
            tagName: 'span',
         });
        nameElement.innerText = value.join(' ');
        return nameElement;
    }

    function fighterImage(src) {
        const attributes = {src};
        const imageElement = createElement({
            tagName: 'img',
            attributes
        });

        return imageElement;
    }
    if (fighter){
        const fightersArray = Object.entries(fighter);
        fighterElement.append(fighterImage(fighter.src));
        const propertiesElement = createElement({
            tagName:'div'
        });
        fightersArray.forEach(value => propertiesElement.append(fighterProperty(value)));
        fighterElement.append(propertiesElement);
    }
    // todo: show fighter info (image, name, health, etc.)
    return fighterElement;
}

export function createFighterImage(fighter) {
    const {source, name} = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}
